import React, { Component } from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateModalData } from "../../Redux/action/updateModalData";
import PropTypes from 'prop-types';

class Modal extends Component {
    state = {

    }

    closeModal = () => {
        const modalObj = {
            title: "",
            visible: false,
            modalData: () => {  }
        }

        this.props.updateModalData(modalObj);
    }

    render () {
        return (
            <div className={`modal show ${this.props.visible ? "" : "hide"}`}>
                <h3 className="title">{this.props.title}</h3>
                <i 
                    className="close"
                    onClick={this.closeModal}
                ></i>
                <div className="modal-body">
                    {this.props.modalData && this.props.modalData(this.state)}
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
      visible: state.updateModalData.visible,
      modalData: state.updateModalData.modalData,
      title: state.updateModalData.title,
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      updateModalData: bindActionCreators(updateModalData, dispatch),
  
    };
  };
  
  const connector = connect(mapStateToProps, mapDispatchToProps);
  
  Modal.propTypes = {
	visible: PropTypes.bool.isRequired,
	modalData: PropTypes.func.isRequired,
	title: PropTypes.string.isRequired,
}
  
  export default connector(Modal);