import React from "react";
import PropTypes from 'prop-types';

function Seat (props) {
    return (
        <td 
            className={`seat ${ props.reserved ? "reserved" : (props.selected ? "selected" : "available") }`}
            onClick={() => {
                if(!props.reserved){
                    props.handleSeatSelect(props.row, props.index, props.seatNumber)

                }
            }}
        >
            <span className="seat-number">{props.seatNumber}</span>
        </td>
    )
}

Seat.propTypes = {
	reserved: PropTypes.bool,
	selected: PropTypes.bool,
	row: PropTypes.string.isRequired,
	index: PropTypes.number.isRequired,
	seatNumber: PropTypes.number.isRequired,
}

export default Seat;