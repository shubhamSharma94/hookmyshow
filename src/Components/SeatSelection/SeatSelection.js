import React, { Fragment } from "react";
import Seat from "./Seat";
import PropTypes from 'prop-types';
import "../../Styles/seat-selection.scss";

function SeatSelection(props) {
	return (
		<Fragment>
			<h4 className="text-center">This side Screen</h4>

			<table className="table  table-bordered seat-container">
				<tbody>
					{
						props.rows.map((row, i) => {
							return (
								<tr className="seat-row" key={i}>
									<td className="seat-row-title">{row}</td>
									{
										props.seatsByRow[row].map((seat, index) => {
											return (
												index === 9 ?
													(
														<Fragment>
															<td className="stair"></td>
															<Seat
																{...seat}
																key={index}
																index={index}
																handleSeatSelect={props.handleSeatSelect}
															/>
														</Fragment>
													) : (
														<Seat
															{...seat}
															key={index}
															index={index}
															handleSeatSelect={props.handleSeatSelect}
														/>
													)

											)
										})
									}
								</tr>
							)
						})
					}
				</tbody>

			</table>
			<label>Selected Seat:  &nbsp;</label><span>{props.selectedSeats.join(", ")}</span>
			<br></br>
			<button
				className="btn btn-primary"
				disabled={props.selectedSeats.length === 0 ? true : false}
				onClick={props.handleConfirmSelection}
			>
				Confirm 
			</button>
			<button
				className="btn btn-danger margin-left-1rem"
				onClick={props.handleCancelSelection}
			>
				Cancel
			</button>
		</Fragment>

	)
}


SeatSelection.propTypes = {
	rows: PropTypes.array,
	selectedSeats: PropTypes.array,
	handleConfirmSelection: PropTypes.func.isRequired,
	handleCancelSelection: PropTypes.func.isRequired,
}

export default SeatSelection;