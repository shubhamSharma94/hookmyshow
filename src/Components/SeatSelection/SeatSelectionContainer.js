import React, { Component } from "react";
import SeatSelection from "./SeatSelection";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateModalData } from "../../Redux/action/updateModalData";
import TheaterList from "../TheaterList/TheaterList";
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { generateDummySeats, getAlphabetArray } from "./helper"
import 'react-toastify/dist/ReactToastify.css';


class SeatSelectionContainer extends Component {
    state = {
        seatsByRow: generateDummySeats(),
        rows: getAlphabetArray(),
        selectedSeats: [],

    }

    handleSeatSelect = (row, index, seatNumber) => {
        let seatsByRow = JSON.parse(JSON.stringify(this.state.seatsByRow));
        seatsByRow[row][index].selected = !seatsByRow[row][index].reserved ? !seatsByRow[row][index].selected : false;

        let selectedSeats = this.state.selectedSeats;
        let seatId = `${row}-${seatNumber}`;
        if (selectedSeats.includes(seatId)) {
            selectedSeats.splice(selectedSeats.indexOf(seatId), 1)
        } else {
            selectedSeats.push(seatId)
        }

        this.setState({
            seatsByRow,
            selectedSeats
        })
    }

    handleCancelSelection = () => {
        const modalObj = {
            title: "Select Theaters",
            visible: true,
            modalData: (data) => { return <TheaterList /> }
        }

        this.props.updateModalData(modalObj);
    }

    handleConfirmSelection = () => {
        const modalObj = {
            title: "",
            visible: false,
            modalData: (data) => { }
        }

        this.props.updateModalData(modalObj);
        toast("Booking Confirmed")
    }

    render() {
        return (
                <SeatSelection
                    {...this.state}
                    handleSeatSelect={this.handleSeatSelect}
                    handleCancelSelection={this.handleCancelSelection}
                    handleConfirmSelection={this.handleConfirmSelection}
                />

        )
    }
}





const mapDispatchToProps = dispatch => {
    return {
        updateModalData: bindActionCreators(updateModalData, dispatch),
    };
};

const connector = connect(null, mapDispatchToProps);

SeatSelectionContainer.propTypes = {
    updateModalData: PropTypes.func,
}


export default connector(SeatSelectionContainer);