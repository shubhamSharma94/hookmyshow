export const generateDummySeats = () => {
    const rows = getAlphabetArray ();

    let seatByRows = {};
    let seatPerRow = 15;
    rows.forEach((key, index) => {
        let seatsPerRow = [];
        for (var i = 1; i <= seatPerRow; i++) {
            seatsPerRow.push(new SeatObj(key, i))
        }

        seatByRows[key] = seatsPerRow
    })

    return seatByRows;

}

export function getAlphabetArray () {
    let alphabets = [];

    for (var i = 65; i < 91; i++) {
        alphabets.push(String.fromCharCode(i))
    }

    return alphabets;

}

export function SeatObj (row, seatNumber) {
    this.row = row;
    this.seatNumber = seatNumber;
    this.selected = false;
    this.reserved = false;
}