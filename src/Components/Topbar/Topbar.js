import React from "react";
import SearchBox from "./SearchBox";
import Branding from "./Branding";
import "../../Styles/topbar.scss";
function Topbar(props) {
  return (
    <div className="topbar">
      <div className="pure-g">
        <div className="pure-u-1-2 pure-u-sm-1-2">
          <Branding />
        </div>
        <div className="pure-u-1-2 pure-u-sm-1-2">
          <SearchBox />
        </div>
      </div>
    </div>
  );
}

export default Topbar;
