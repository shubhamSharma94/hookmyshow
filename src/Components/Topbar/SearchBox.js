import React from "react";
import axios from "axios";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateMovieList } from '../../Redux/action/updateMovieList';
import PropTypes from 'prop-types';

class SearchBox extends React.Component {
  state = {
    searchText: ""
  };

  handleChange = value => {
    this.setState({
      searchText: value
    }, () => {
      axios.get(`https://www.omdbapi.com/?apikey=a95a07d7&s=${this.state.searchText}`).then(resp => {
        if (resp.data.Response === "True") {
          console.log(resp.data);
          this.props.updateMovieList(resp.data.Search);
        }

      })
    });
  };

  componentDidMount () {
    
  }



  render() {
    return (
      <input
        type="text"
        className="search-box"
        value={this.state.searchText}
        placeholder="Search for Movies, Events, Plays, Sports and Activities"
        onChange={e => {
          this.handleChange(e.target.value);
        }}
      />
    );
  }
}



const mapStateToProps = state => {
  return {
      movieList: state.updateMovieList.movieList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateMovieList: bindActionCreators(updateMovieList, dispatch),
  };
};

const connector = connect(mapStateToProps, mapDispatchToProps);


SearchBox.propTypes = {
	movieList: PropTypes.array,
}

export default connector(SearchBox);