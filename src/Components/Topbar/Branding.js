import React from "react";
import ReactSVG from 'react-svg'
function Branding () {
  return (
    <div className="brand">
      <ReactSVG className="brand-image" src="./images/movie-clapper-open.svg" />
      <span className="brand-label">hook
        <span className="red-bg-text">my</span>
      show</span>
    </div>
  )
}


export default Branding;