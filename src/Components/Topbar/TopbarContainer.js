import React, { Component } from "react";
import Topbar from "./Topbar";

class TopbarContainer extends Component {
  state = {};

  render() {
    return <Topbar />;
  }
}

export default TopbarContainer;
