import React from "react";
import PropTypes from 'prop-types';

function TheaterItem (props) {
  return (
    <div 
      className="theater-item"
      onClick={props.handleTheaterClick}
    >
      {
        props.name
      }
    </div>
  )
}

TheaterItem.propTypes = {
  handleTheaterClick: PropTypes.func.isRequired,
}

export default TheaterItem;