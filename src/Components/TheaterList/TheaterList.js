import React , { Component } from "react";
import TheaterItem from "./TheaterItem";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateModalData } from "../../Redux/action/updateModalData";
import SeatSelectionContainer from "../SeatSelection/SeatSelectionContainer";
import PropTypes from 'prop-types';

class TheaterList extends Component {
  state = {
    theaterList: [
      {
        name: "Cinepolis: Binnypet Mall",
        id: 1,
      },
      {
        name: "Cinepolis: Forum Shantiniketan, Bengaluru",
        id: 2,
      },
      {
        name: "Cinepolis: Orion East Mall, Banaswadi",
        id: 3,
      },
      {
        name: "Cinepolis: Royal Meenakshi Mall",
        id: 4,
      },
      {
        name: "Gopalan Cinemas: Bannerghatta Road",
        id: 5,
      },
      {
        name: "INOX Lido: Off MG Road, Ulsoor",
        id: 6,
      },
      
    ]
  }

  handleTheaterClick = () => {
    const modalObj = {
      title: "Select Seats",
      visible: true,
      modalData: (data) => { return <SeatSelectionContainer /> }
    }

    this.props.updateModalData(modalObj);
  }


  render () {
    return (
      <div className={`theater-list`}>
        {
          this.state.theaterList && this.state.theaterList.map(item => {
            return (
              <TheaterItem 
                key={item.id}
                {...item}
                handleTheaterClick={this.handleTheaterClick}
              />
            )
          })
        }
        
      </div>
    )
  }
}




const mapDispatchToProps = dispatch => {
  return {
    updateModalData: bindActionCreators(updateModalData, dispatch),
  };
};

const connector = connect(null, mapDispatchToProps);

TheaterList.propTypes = {
	updateModalData: PropTypes.func.isRequired,
}

export default connector(TheaterList);