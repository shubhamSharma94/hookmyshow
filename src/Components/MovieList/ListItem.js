import React from "react";
import PropTypes from 'prop-types';

function ListItem (props) {
  return (
    <div 
      className="movie-list-item pure-u-sm-1" 
      onClick={props.handleClick}
      style={props.style}
    >
      {
        props.Poster === "N/A" ? (
          <span className="not-available">{props.Poster}</span>
        ) : <img src={props.Poster} alt={props.Title} />
      }
      
      <span className="poster-title">{props.Title}</span>
    </div>
  )
}

ListItem.propTypes = {
	handleClick: PropTypes.func.isRequired,
  Poster: PropTypes.string,
  Title: PropTypes.string.isRequired,
}

export default ListItem;