import React, { Component } from "react";
import ListItem from "./ListItem";
import axios from "axios";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateMovieList } from '../../Redux/action/updateMovieList';
import { updateModalData } from "../../Redux/action/updateModalData";
import TheaterList from "../TheaterList/TheaterList";
import PropTypes from 'prop-types';
import "../../Styles/movie-list.scss";

import ReactCSSTransitionGroup from 'react-addons-css-transition-group'; // ES6


const getMovieList = () => {
  return axios.get(`https://www.omdbapi.com/?apikey=a95a07d7&s=avenger`)
} 

class ListContainer extends Component {
  state = {

  }

  componentDidMount () {
    getMovieList().then(resp => {
      if (resp.data.Response === "True") {
        this.props.updateMovieList(resp.data.Search);
      }
    })
  }

  handleClick = (id) => {
    //id to be used for fetching of the theaters available for the selected movie
    // after fetching data

    const modalObj = {
      title: "Select Theaters",
      visible: true,
      modalData: (data) => { return <TheaterList /> }
    }

    this.props.updateModalData(modalObj);
  }

  render () {

    const items = this.props.movieList && this.props.movieList.map((item, i) => {
      return (
        <ListItem 
          key={item.imdbID}
          {...item}
          handleClick={this.handleClick}
          style={{"transitionDelay": `${ i * 0.05 }s` }}
        />
      )
    })

    return (
      <div className="movie-list pure-g">
        <ReactCSSTransitionGroup 
          transitionName="fade" 
          transitionAppear={true}
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}  
          transitionAppearTimeout={500}
        >
        {
          items
        }
        </ReactCSSTransitionGroup>
        

      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
      movieList: state.updateMovieList.movieList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateMovieList: bindActionCreators(updateMovieList, dispatch),
    updateModalData: bindActionCreators(updateModalData, dispatch),

  };
};

const connector = connect(mapStateToProps, mapDispatchToProps);

ListContainer.propTypes = {
	movieList: PropTypes.array.isRequired,
}

export default connector(ListContainer);