import React, { Component } from "react";
import TopbarContainer from "./Topbar/TopbarContainer";
import ListContainer from "./MovieList/ListContainer";
import Modal from "./Modal/Modal";
import { ToastContainer } from 'react-toastify';
import "../Styles/main.scss";



class App extends Component {
  state = {};

  render() {
    return (
      <div className="app">
        <TopbarContainer />
        <ToastContainer />
        <div className="slate">
            <ListContainer />
            <Modal />
        </div>
      </div>
    );
  }
}

export default App;