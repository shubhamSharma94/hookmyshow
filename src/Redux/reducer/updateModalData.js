import {
	TOGGLE_MODAL,
} from "../Constants.js";


const initialModalData = {
    visible: false,
    modalData: () => {},
    title: "Modal"
  }
  
  
  export const updateModalData = (state = initialModalData, action = {}) => {
    switch (action.type) {
      case TOGGLE_MODAL:
        return Object.assign({}, state, {
            visible: action.payload.visible,
            modalData: action.payload.modalData,
            title: action.payload.title,
            
        })
      default:
        return state
    }
  }