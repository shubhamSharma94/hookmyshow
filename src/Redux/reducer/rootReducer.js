import { combineReducers } from 'redux';
import { updateMovieList } from "./updateMovieList";
import { updateModalData } from "./updateModalData";

const rootReducer = combineReducers({
    updateMovieList,
    updateModalData
});

export default rootReducer;