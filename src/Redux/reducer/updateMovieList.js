import {
	UPDATE_MOVIE_LIST,
} from "../Constants.js";


const initialSMovieList = {
    movieList: [],
  }
  
  
  export const updateMovieList = (state = initialSMovieList, action = {}) => {
    switch (action.type) {
      case UPDATE_MOVIE_LIST:
        return Object.assign({}, state, {
          movieList: action.payload,
        })
      default:
        return state
    }
  }