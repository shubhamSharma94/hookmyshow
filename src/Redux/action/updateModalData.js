import { TOGGLE_MODAL } from "../Constants.js";

export const updateModalData = (data) => {
  return {
    type: TOGGLE_MODAL,
    payload: data,

  }
}
