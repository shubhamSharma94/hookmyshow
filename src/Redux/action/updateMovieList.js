import { UPDATE_MOVIE_LIST } from "../Constants.js";

export const updateMovieList = (data) => {
  return {
    type: UPDATE_MOVIE_LIST,
    payload: data,

  }
}
